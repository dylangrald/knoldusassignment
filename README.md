Run with `sbt run`

Test with `sbt test`

There is a POST endpoint at `/process-sale` that accepts sales with a JSON body. To specify the following price schema
from the assignment and the sale "ABCD", perform an HTTP POST at http://localhost:9000/process-sale and use the
following JSON:

```json
{
   "itemPrices": {
     "A": {"perItemPrice": 2.00, "volumePrice": {"number": 4, "price": 7.00 } },
     "B": {"perItemPrice": 12.00 },
     "C": {"perItemPrice": 1.25, "volumePrice": {"number": 6, "price": 6.00 } },
     "D": {"perItemPrice": 0.15 }
   },
   "items": ["A", "B", "C", "D"]
}```



Similarly, to process the sale "ABCDABAA", use the JSON:

```json
{
   "itemPrices": {
     "A": {"perItemPrice": 2.00, "volumePrice": {"number": 4, "price": 7.00 } },
     "B": {"perItemPrice": 12.00 },
     "C": {"perItemPrice": 1.25, "volumePrice": {"number": 6, "price": 6.00 } },
     "D": {"perItemPrice": 0.15 }
   },
   "items": ["A", "B", "C", "D", "A", "B", "A", "A"]
}```