package controllers

import akka.stream.ActorMaterializer
import json.SaleJsonValidator
import org.scalatest.mockito.MockitoSugar
import org.scalatestplus.play._
import org.scalatestplus.play.guice._
import play.api.libs.json.{JsValue, Json}
import play.api.test._
import play.api.test.Helpers._
import play.api.test.CSRFTokenHelper._
import services.{ItemPrice, SaleTerminalService}
import org.mockito.Mockito._
import org.scalatest.BeforeAndAfterEach
import play.api.Application
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.inject.bind
import play.api.mvc.Result

import scala.concurrent.Future

class SaleControllerSpec extends PlaySpec with GuiceOneAppPerTest with MockitoSugar with BeforeAndAfterEach {

  private var mockSaleTerminalService: SaleTerminalService = _
  private var mockSaleJsonValidator: SaleJsonValidator = _

  override def fakeApplication(): Application = new GuiceApplicationBuilder()
    .overrides(bind[SaleJsonValidator].toInstance(mockSaleJsonValidator))
    .overrides(bind[SaleTerminalService].toInstance(mockSaleTerminalService))
    .build()

  override def beforeEach(): Unit = {
    mockSaleTerminalService = mock[SaleTerminalService]
    mockSaleJsonValidator = mock[SaleJsonValidator]
  }

  "processSale" should {
    "return the value of the total sale when given valid inputs" in {
      val validJson = someJson()
      val saleRequest = someSaleRequest
      val total = 22.2

      when(mockSaleJsonValidator.parseJson(validJson)).thenReturn(Some(saleRequest))
      when(mockSaleTerminalService.total(saleRequest.itemPrices, saleRequest.items)).thenReturn(Some(total))
      val saleController = app.injector.instanceOf[SaleController]
      val request = FakeRequest(POST, "/process-sale").withJsonBody(validJson)
      val response = saleController.processSale.apply(request)

      status(response) mustEqual OK
      contentAsJson(response) mustEqual Json.obj("total" -> total)
    }

    "return bad request when given invalid sale request JSON" in {
      val json = someJson()

      when(mockSaleJsonValidator.parseJson(json)).thenReturn(None)
      val saleController = app.injector.instanceOf[SaleController]
      val request = FakeRequest(POST, "/process-sale").withJsonBody(json)
      val response = saleController.processSale.apply(request)

      status(response) mustEqual BAD_REQUEST
      contentAsJson(response) mustEqual Json.obj("error" -> "invalid sale data")
    }

    "return bad request when total fails" in {
      val validJson = someJson()
      val saleRequest = someSaleRequest

      when(mockSaleJsonValidator.parseJson(validJson)).thenReturn(Some(saleRequest))
      when(mockSaleTerminalService.total(saleRequest.itemPrices, saleRequest.items)).thenReturn(None)
      val saleController = app.injector.instanceOf[SaleController]
      val request = FakeRequest(POST, "/process-sale").withJsonBody(validJson)
      val response = saleController.processSale.apply(request)

      status(response) mustEqual BAD_REQUEST
      contentAsJson(response) mustEqual Json.obj("error" -> "invalid sale data: unable to total")
    }
  }

  private def someJson(): JsValue = {
    Json.obj("foo" -> true)
  }

  private def someSaleRequest: SaleRequest = {
    SaleRequest(
      Map("A" -> ItemPrice(2, None)),
      List("A", "B")
    )
  }
}