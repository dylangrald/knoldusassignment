package services

import org.scalatestplus.play.PlaySpec

class SaleTerminalServiceSpec extends PlaySpec {
  "totalPricing" should {
    val saleTerminalService = new SaleTerminalService()
    val aPrice = ItemPrice(2, Some(VolumePrice(4, 7)))
    val bPrice = ItemPrice(12, None)
    val cPrice = ItemPrice(1.25, Some(VolumePrice(6, 6)))
    val dPrice = ItemPrice(0.15, None)
    val pricing = Map(
      "A" -> aPrice,
      "B" -> bPrice,
      "C" -> cPrice,
      "D" -> dPrice
    )

    "return $32.40 for ABCDABAA" in {
      val items = "ABCDABAA"
      val total = totalPricing(items, saleTerminalService)

      total mustEqual Some(32.40)
    }

    "return $7.25 for CCCCCCC" in {
      val items = "CCCCCCC"
      val total = totalPricing(items, saleTerminalService)

      total mustEqual Some(7.25)
    }

    "return $15.40 for ABCD" in {
      val items = "ABCD"
      val total = totalPricing(items, saleTerminalService)

      total mustEqual Some(15.40)
    }

    "return None for ABCDE" in {
      val items = "ABCDE"
      val total = totalPricing(items, saleTerminalService)

      total mustEqual None
    }

    def totalPricing(itemsString: String, saleTerminalService: SaleTerminalService): Option[Double] = {
      val items = itemsString.split("").toList
      saleTerminalService.total(pricing, items)
    }
  }
}
