package json

import controllers.SaleRequest
import org.scalatestplus.play.PlaySpec
import play.api.libs.json.Json
import services.{ItemPrice, VolumePrice}

class SaleJsonValidatorSpec extends PlaySpec {

  "parseJson" should {
    "return valid SaleRequest item when given correct json" in {
      val validJson =
        """
          |{
          |   "itemPrices": {
          |     "A": {"perItemPrice": 2.50 },
          |     "B": {"perItemPrice": 1.00, "volumePrice": {"number": 4, "price": 3.00 } }
          |   },
          |   "items": ["A", "B", "B", "A"]
          |}
        """.stripMargin

      val parsedSaleRequest = new SaleJsonValidator().parseJson(Json.parse(validJson))

      val expected = SaleRequest(
        Map(
          "A" -> ItemPrice(2.50, None),
          "B" -> ItemPrice(1.00, Some(VolumePrice(4, 3.00)))
        ),
        List("A", "B", "B", "A")
      )
      parsedSaleRequest.get mustEqual expected
    }

    "return None when given incorrect json schema" in {
      val validJson =
        """
          |{
          |   "itemPrices": {
          |     "A": {"perItemPrice": 2.50 },
          |     "B": {"perItemPrice": 1.00, "volumePrice": 3.0 }
          |   },
          |   "items": ["A", "B", "B", "A"]
          |}
        """.stripMargin

      val parsedSaleRequest = new SaleJsonValidator().parseJson(Json.parse(validJson))

      val expected = None
      parsedSaleRequest mustEqual expected
    }

    "return None when given item not in item prices Map" in {
      val validJson =
        """
          |{
          |   "itemPrices": {
          |     "A": {"perItemPrice": 2.50 }
          |   },
          |   "items": ["A", "B"]
          |}
        """.stripMargin

      val parsedSaleRequest = new SaleJsonValidator().parseJson(Json.parse(validJson))

      val expected = None
      parsedSaleRequest mustEqual expected
    }
  }
}
