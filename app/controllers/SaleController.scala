package controllers

import javax.inject.Inject
import json.JsonFormatters._
import json.SaleJsonValidator
import play.api.libs.json.Json
import play.api.mvc._
import services.{ItemPrice, SaleTerminalService}


class SaleController @Inject()(cc: ControllerComponents, saleTerminalService: SaleTerminalService, saleJsonValidator: SaleJsonValidator)
    extends AbstractController(cc) {

  def processSale = Action { request =>
    val requestJson = request.body.asJson
    if (requestJson.isEmpty) {
      BadRequest(Json.obj("error" -> "invalid JSON"))
    } else {
      val saleRequestOption = saleJsonValidator.parseJson(requestJson.get)
      if (saleRequestOption.isEmpty) {
        BadRequest(Json.obj("error" -> "invalid sale data"))
      } else {
        val saleRequest = saleRequestOption.get
        val total = saleTerminalService.total(saleRequest.itemPrices, saleRequest.items)
        if (total.isEmpty) {
          BadRequest(Json.obj("error" -> "invalid sale data: unable to total"))
        } else {
          Ok(Json.obj("total" -> total.get))
        }
      }
    }
  }
}

case class SaleRequest(itemPrices: Map[String, ItemPrice], items: List[String])