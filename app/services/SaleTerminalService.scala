package services

import javax.inject.Inject

import scala.annotation.tailrec


class SaleTerminalService @Inject()() {

  def total(itemPrices: Map[String, ItemPrice], items: List[String]): Option[Double] = {
    @tailrec
    def totalItemByVolume(itemsRemaining: List[String], pricePerItem: Double, pricePerVolume: VolumePrice, total: Double): Double = {
      if (itemsRemaining.isEmpty || itemsRemaining.length < pricePerVolume.number) {
        val totalOfRemaining = itemsRemaining.length * pricePerItem
        totalOfRemaining + total
      } else {
        totalItemByVolume(itemsRemaining.drop(pricePerVolume.number), pricePerItem, pricePerVolume, total + pricePerVolume.price)
      }
    }

    val allItemsValid = items.forall(i => itemPrices.contains(i))

    if (!allItemsValid) {
      None
    } else {
      val itemsGrouped = items.groupBy(t => t)
      val totalCostByItem = itemsGrouped.map {
        case (t, itemsCheckedOut) =>
          val pricingForItem = itemPrices(t)
          if (pricingForItem.volumePrice.isDefined) {
            totalItemByVolume(itemsCheckedOut, pricingForItem.perItemPrice, pricingForItem.volumePrice.get, 0)
          } else {
            itemsCheckedOut.size * pricingForItem.perItemPrice
          }
      }
      Some(totalCostByItem.sum)
    }
  }
}

case class ItemPrice(perItemPrice: Double, volumePrice: Option[VolumePrice])
case class VolumePrice(number: Int, price: Double)
