package json

import controllers.SaleRequest
import javax.inject.Inject
import play.api.libs.json.JsValue
import json.JsonFormatters._

class SaleJsonValidator @Inject()() {
  def parseJson(jsonBody: JsValue): Option[SaleRequest] = {
    val saleRequestOpt = jsonBody.validate[SaleRequest].asOpt
    if (saleRequestOpt.isDefined) {
      val saleRequest = saleRequestOpt.get
      val allItemsHavePrices = saleRequest.items.forall { item =>
        saleRequest.itemPrices.get(item).isDefined
      }
      if (allItemsHavePrices) {
        saleRequestOpt
      } else {
        None
      }
    } else {
      None
    }
  }
}
