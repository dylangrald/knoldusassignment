package json

import controllers.SaleRequest
import play.api.libs.json.{Json, OFormat}
import services.{ItemPrice, VolumePrice}

object JsonFormatters {
  implicit val volumePrice: OFormat[VolumePrice] = Json.format[VolumePrice]
  implicit val itemPriceFormat: OFormat[ItemPrice] = Json.format[ItemPrice]
  implicit val saleRequestFormat: OFormat[SaleRequest] = Json.format[SaleRequest]
}
