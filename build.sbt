name := "knoldusassignment"

version := "0.0.1-SNAPSHOT"

scalaVersion := "2.12.6"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

libraryDependencies ++= Seq(
  ws,
  guice,
  "net.codingwell" %% "scala-guice" % "4.2.1",

  "org.scalatestplus.play" %% "scalatestplus-play" % "3.1.2" % Test,
  "org.mockito" %% "mockito-scala" % "1.1.3" % Test
)
